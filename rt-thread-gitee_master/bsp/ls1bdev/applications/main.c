/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-05-10     zhuangwei    first version
 */

#include <rtthread.h>
#include "ls1b_gpio.h"
void LedBlinkTest(void);

int main(int argc, char **argv)
{
    rt_kprintf("Base LS1B Hello_World Project !!\n");
    // while (1)
    // {
    //    //LedBlinkTest();
    // }
    
    return 0;
}

void LedBlinkTest(void)
{
    int i;
    unsigned int gpio = 50; // 智龙首发版、 v2.0、 v2.1、 v3.0 都有这个 led
    // 初始化
    rt_kprintf("LedBlinkTest\r\n");
    gpio_init(gpio, gpio_mode_output);
    //rt_kprintf("gpio_init\r\n");
    // 输出 10 个矩形波，如果 gpio52 上有 led，则可以看见 led 闪烁 10 次
    {
        gpio_set(gpio, gpio_level_low);
        for ( long int i = 0; i < 0xffff; i++)
        {
            for ( long int j = 0; j < 0xffff; j++){
                ;
            }        
        }
        rt_kprintf("gpio_level_low");
        gpio_set(gpio, gpio_level_high);
        for ( long int i = 0; i < 0xffff; i++)
        {
            for ( long int j = 0; j < 0xffff; j++){
                ;
            }
        }
        rt_kprintf("gpio_level_high");
    }
    return;
}